create table region(
   id int not null primary key,
   name VARCHAR(250)
);

insert into region(
    id, name
)
values(1, "India"), (2, "Seattle"), (3, "San Fransico"), (4, "Atlanta")


create table user(
    id int not null primary key,
    username varchar(250),
    email varchar(250),
    mobileNumber varchar(50),
    region_id int,
    foreign key(region_id) references region(id) ON DELETE CASCADE
);

insert into user(id, username, email, mobileNumber, region_id)
values(1, "Mahesh", "mahesh@gmail.com", "8008800888", 1),
      (2, "Nani", "nani@gmail.com", "9247904049", 2),
      (3, "Nagendra", "nagendra@gmail.com", "8886421556", 3),
      (4, "Sravan", "sravan@gmail.com", "8556541231", 4)

create table category(
    id int not null primary key,
    category_type varchar(250)
);

insert into category(id, category_type)
values(1, "business"), (2, "job"), (3, "greetings");

create table post(
    id int not null primary key,
    user_id int,
    category_id int,
    post_title varchar(250),
    post_detail text,
    location varchar(250),
    region varchar(250),
    foreign key(user_id) references user(id) ON DELETE CASCADE,
    foreign key(category_id) references category(id) ON DELETE CASCADE
);


insert into post(
    id, user_id, category_id, post_title, post_detail, location, region
)
values(1, 1, 1, "land_selling", "there 2 Acres of land near Vizag", "vizag", "India"),
      (2, 2, 2, "Job_hiring", "required full stack developer with or without experience", "Santa Clara", "San Fransisco");