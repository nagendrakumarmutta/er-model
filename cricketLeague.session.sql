create table season(
    id int not null primary key,
    season int,
    season_start date,
    season_end date
);

create table teams(
    id int not null primary key,
    team_name varchar(250)
);

create TABLE player_category(
    id int not null primary key,
    name varchar(250),
    role varchar(250),
    team_id int,
    foreign key(team_id) references teams(id) ON DELETE CASCADE
);


create table matches (
    id int not null primary key,
    team1 varchar(120),
    team2 varchar(120),
    umpire varchar(120),
    season_id int,
    foreign key(season_id) references season(id) ON DELETE CASCADE
);

create table pointsTable (
    team_name varchar(120),
    won int,
    lost int,
    no_result int,
    points int
);



insert into season (id,season,season_start,season_end)
values
(1, 2008,'2008-03-25','2008-05-10'),
(2, 2009,'2009-03-22','2009-05-08'),
(3, 2010,'2010-03-28','2010-05-13'),
(4, 2011,'2011-03-30','2011-05-15');



insert into teams(id, team_name)
values(1,"Chennai Super Kings"),
(2,"Mumbai Indians"),
(3,"Sunrisers Hyderabad"),
(4,"Royal ChallengersBanglore");

insert into player_category(
    id, name, role, team_id
)
values(1, "Dhoni", "Batsman", 1),
      (2, "Sachin", "Batsman", 2),
      (3, "Bhuvneshwar Kumar", "Bowler", 3),
      (4, "David Willey", "All Rounder", 4);


insert into matches(id, team1, team2, umpire, season_id)
values(1, "CSK", "RR", "Sundaram Ravi", 2),
       (2, "MI", "RCB", "John Williams", 3);

insert into pointsTable
(team_name,won,lost,no_result,points)
values
("Chennai Super Kings",2,0,0,4),
("Mumbai Indians",1,1,0,2),
("Royal Challengers Banglore",1,1,0,2),
("Sunrisers Hyderabad",0,2,0,0);


