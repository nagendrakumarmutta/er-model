create TABLE Medical_centre(
    id int not null primary key,
    name varchar(250),
    address text,
    phone_number varchar(50)
);

insert into Medical_centre(id, name, address, phone_number)
values(1, "Apollo", "Vizag", "8008753537");

create table Doctor(
    employee_id int not null primary key,
    name varchar(250),
    designation varchar(250),
    centre_id int,
    foreign key(centre_id) references Medical_centre(id) ON DELETE CASCADE
);

insert into Doctor(employee_id, name, designation, centre_id)
values("001", "Dr.Ramesh", "Cardiologist", 1),
      ("002", "Dr.Suresh", "Orthopedic", 1),
      ("003", "Dr.Jaya", "Gynacologist",1);

create table Patient(
    patient_id int not null primary key,
    name varchar(250),
    age int,
    disease varchar(250)
);

insert into Patient(
    patient_id, name, age, disease
)
values("101", "Ramu", 35, "cancer"),
      ("102", "Ravi", 25, "Obesity"),
      ("103", "Revanth", 21, "Thyroid");

create table Junction_table_onVisit(
    id int not null primary key,
    Doctor_id int,
    Patient_id int,
    disease varchar(250),
    foreign key(Doctor_id) references Doctor(employee_id) ON DELETE CASCADE,
    foreign key(Patient_id) references Patient(patient_id) ON DELETE CASCADE
);

insert into Junction_table_onVisit(id, Doctor_id, Patient_id, disease)
values(1, "001", "101", "cancer, heartAttack"),
      (2, "001", "102", "lungs problem, stomach"),
      (3, "002", "103", "viral fever heavy weight"),
      (4, "002", "101", "cold cough corona"),
      (5, "003", "102", "lungs problem, stomach"),
      (6, "003", "103", "viral fever heavy weight");